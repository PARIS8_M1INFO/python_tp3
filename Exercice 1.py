print(" ############################################################# EXERCICE 1")
#--------------------------------------------------------------------------------
#------- ***   Exercice 1
#--------------------------------------------------------------------------------


def compte_mots(s):
    p, nb_mots = ' ', 0
    for char in s:
        nb_mots += int(p == ' ' and char != ' ')
        p = char
    return nb_mots

print(compte_mots(''))
print(compte_mots('il ingurgite impunément un iguane.'))
print(compte_mots('coursdeprogrammation'))
print(compte_mots('Attention aux espaces consécutifs ou terminaux'))
print(compte_mots('cours programation'))