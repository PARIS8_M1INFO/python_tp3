print(" ############################################################# EXERCICE 2")
#--------------------------------------------------------------------------------
#------- ***   Exercice 2
#--------------------------------------------------------------------------------


def remplace_multiple(s1,s2,n):
    x=1
    i=0
    while n<len(s1):
        listeS1 = list(s1)
        listeS1[n] = s2[i]
        s1 = "".join(listeS1)
        n = n * (x + 1)
        i = i+1

    return s1 + s2[i:]


print(remplace_multiple('abacus','oiseau',2))
print(remplace_multiple('','',2))
print(remplace_multiple('hirondelles','nid',3))
print(remplace_multiple('hello','bonjour',2))
print(remplace_multiple('bonjour','bonsoir',3))