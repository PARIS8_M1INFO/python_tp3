print(" ############################################################# EXERCICE 3")
#---------------------------------------------------------------------------------
#------- ***   Exercice 3
#---------------------------------------------------------------------------------



print(" ######################### Enoncé 1")
#-------------------------------------------
#------- ***   Enoncé 1
#-------------------------------------------


import time

def termeU(n):
    un = 1
    for i in range(n + 1):
        un = un * (2 ** i) + i
    return un
print(termeU(0))
print(termeU(1))
print(termeU(4))
print(termeU(5))
print(termeU(10))


print(" ######################### Enoncé 2")
#-------------------------------------------
#------- ***   Enoncé 2
#-------------------------------------------



def serie(p):
    sp = 0
    for n in range(p + 1):
        sp = sp + termeU(n)
    return sp

print(serie(0))
print(serie(1))
print(serie(4))
print(serie(5))
print(serie(10))



print(" ######################### Enoncé 3")
#-------------------------------------------
#------- ***   Enoncé 3
#-------------------------------------------



def serie_v2(p):
    sp = 0
    un = 1
    for n in range(p + 1):
        un = un * (2 ** n) + n
        sp = sp + un
    return sp

print(serie_v2(0))
print(serie_v2(1))
print(serie_v2(4))
print(serie_v2(5))
print(serie_v2(10))


print(" ######################### Enoncé 3")
#-------------------------------------------
#------- ***   Enoncé 3
#-------------------------------------------



print(" ######################### Enoncé 5")
#-------------------------------------------
#------- ***   Enoncé 5
#-------------------------------------------

depart = time.perf_counter()
print(serie(0))
arrivee = time.perf_counter()
print('temps passe en secondes dans la fontion serie :', arrivee - depart)


depart = time.perf_counter()
print(serie_v2(0))
arrivee = time.perf_counter()
print('temps passe en secondes dans la fontion serie_v2 :', arrivee - depart)

# L'algorithme le plus efficace est l'algorithme serie_v2 puisque il s'execute
# en moins de temps par rapport à l'algorithme serie.