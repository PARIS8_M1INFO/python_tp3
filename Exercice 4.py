def factorielle(n):
    if n < 2:
        return 1
    else:
        return n * factorielle(n-1)

print(factorielle(1))
print(factorielle(2))
print(factorielle(3))
print(factorielle(4))
print(factorielle(5))